/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.datalab1;

/**
 *
 * @author Paweena Chinasri
 */
public class MergeSortedArray {
    public static void main(String[] args){
        int[] num1 = {1,2,3,0,0,0};
        int[] num2 = {2,5,6};
        int m = 3;
        int n = 3;
        int[] nums3 = new int[m+n];
        int i = 0;
        int j = 0;
        int k = 0;
        while(i<m&&j<n){
            if(num1[i]<num2[j]){
                nums3[k++] = num1[i++];
            }else{
                nums3[k++] = num2[j++];
            }
        }
        while(i<m){
            nums3[k++] = num1[i++];
        }
        while(j<n){
            nums3[k++] = num2[j++];
        }
        for(int l=0; l<nums3.length; l++){
            System.out.print(nums3[l]+" ");
        }
        }
}

