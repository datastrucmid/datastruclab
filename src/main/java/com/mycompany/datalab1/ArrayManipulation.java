/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.datalab1;

/**
 *
 * @author Paweena Chinasri
 */
public class ArrayManipulation {

    public static void main(String[] args) {
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
         
//Print the elements of the "numbers" array using a for loop
        for(int i=0; i<numbers.length; i++){
            System.out.print(numbers[i]+" ");
        }
        System.out.println();
//Print the elements of the "names" array using a for-each loop.
        for(String name: names){
            System.out.print(name+" ");
        }
        System.out.println();
//Initialize the elements of the "values" array with any four decimal values of your choice.
        values[0] = 1.2;
        values[1] = 2.2;
        values[2] = 3.3;
        values[3] = 4.4;
//Calculate and print the sum of all elements in the "numbers" array.
        int sum = 0;
        for(int i=0; i<numbers.length; i++){
            sum += numbers[i];
        }
        System.out.println(sum);
//Find and print the maximum value in the "values" array.
        double max = values[0];
        for(int i=0; i<values.length; i++){
            if(values[i]>max){
                max = values[i];
            }
        }
        System.out.println(max);
//Create a new string array named "reversedNames" with the same length as the "names" array.
        String[] reversedNames = new String[names.length];
        for(int i=0; i<names.length; i++){
            reversedNames[i] = names[names.length-1-i];
        }
        for(String name: reversedNames){
            System.out.print(name+" ");
        }
        System.out.println();
//BONUS: Sort the "numbers" array in ascending order using any sorting algorithm of your choice
        for(int i=0; i<numbers.length; i++){
            int minindex = i;
            for(int j=i+1; j<numbers.length; j++){
                if(numbers[i]>numbers[minindex]){
                    minindex = j;
                }
            }
                    int temp = numbers[i];
                    numbers[i] = numbers[minindex];
                    numbers[minindex] = temp;
                }
                for(int i=0; i<numbers.length; i++){
                    System.out.print(numbers[i]+" ");
                }
                System.out.println();
            }

    }
